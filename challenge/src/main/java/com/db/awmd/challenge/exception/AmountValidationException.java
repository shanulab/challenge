package com.db.awmd.challenge.exception;

public class AmountValidationException extends RuntimeException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public AmountValidationException(String message) {
	    super(message);
	  }
	
	public AmountValidationException(String message,Throwable details) {
	    super(message,details);
	  }

}
