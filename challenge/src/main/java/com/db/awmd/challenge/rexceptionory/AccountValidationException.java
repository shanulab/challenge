package com.db.awmd.challenge.rexceptionory;

public class AccountValidationException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public AccountValidationException(String message) {
	    super(message);
	  }

}
