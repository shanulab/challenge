package com.db.awmd.challenge.repository;

import com.db.awmd.challenge.domain.Account;
import com.db.awmd.challenge.exception.AmountValidationException;
import com.db.awmd.challenge.exception.DuplicateAccountIdException;
import com.db.awmd.challenge.rexceptionory.AccountValidationException;
import com.db.awmd.challenge.service.AccountsService;
import com.db.awmd.challenge.service.EmailNotificationService;
import com.db.awmd.challenge.web.AccountsController;

import java.math.BigDecimal;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class AccountsRepositoryInMemory implements AccountsRepository {

	private final Map<String, Account> accounts = new ConcurrentHashMap<>();
	private Logger log = LoggerFactory.getLogger(AccountsRepositoryInMemory.class);
	private final EmailNotificationService emailNotificationService ;

	@Autowired
	public AccountsRepositoryInMemory(EmailNotificationService emailNotificationService) {
		this.emailNotificationService = emailNotificationService;
	}
	@Override
	public void createAccount(Account account) throws DuplicateAccountIdException {
		Account previousAccount = accounts.putIfAbsent(account.getAccountId(), account);
		if (previousAccount != null) {
			throw new DuplicateAccountIdException("Account id " + account.getAccountId() + " already exists!");
		}
	}

	@Override
	public Account getAccount(String accountId) {
		return accounts.get(accountId);
	}

	@Override
	public void clearAccounts() {
		accounts.clear();
	}

	@Override
	public String transferAmount(String fromAccountId, String toAccountId, BigDecimal amount)throws AccountValidationException {
		String messageDetails=null;
		if (amount.compareTo(BigDecimal.ZERO) < 0) {
			throw new AmountValidationException("Amount  " + amount + " can not be negative !");
		}
		Account fromAccount = accounts.get(fromAccountId);
		Account toAccount = accounts.get(toAccountId);
		if (fromAccount == null || toAccount == null) {
			throw new AccountValidationException(fromAccount.toString() == null ? fromAccountId : toAccountId + " Account Id  doest not Exist!");
		}
		BigDecimal FromBalance = fromAccount.getBalance();
		BigDecimal toBalance = toAccount.getBalance();
		if (FromBalance.compareTo(amount) >= 0) {
			fromAccount.setBalance(FromBalance.subtract(amount));
			messageDetails="Amount "+amount+ " tranfered to Account No. "+toAccount.getAccountId();
			emailNotificationService.notifyAboutTransfer(fromAccount, messageDetails);
			toAccount.setBalance(toBalance.add(amount));
			messageDetails="Amount "+amount+ " received from Account No. "+fromAccount.getAccountId();
			emailNotificationService.notifyAboutTransfer(toAccount, messageDetails);
			
			log.info("fromAccount :" + fromAccount.getBalance());
			log.info("toAccount   :" + toAccount.getBalance());
		} else {
			throw new AmountValidationException(
					"Not Enough Balance in Account "+fromAccountId);
		}

		return messageDetails;
	}

}
